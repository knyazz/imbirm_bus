#coding: utf-8
#from lxml import etree
from django.conf import settings
from django.db import models


class  Barcode(models.Model):
    code = models.CharField(max_length=255, editable=False)

    class Meta:
        verbose_name=u'Штрихкод'
        verbose_name_plural = verbose_name + u'ы'


class WarehouseProduct(models.Model):
    wp_id = models.CharField(max_length=255, editable=False)
    description = models.CharField(max_length=255, editable=False)
    ref = models.CharField(max_length=255, editable=False)
    qty = models.PositiveSmallIntegerField(default=0, editable=False)
    amount = models.PositiveSmallIntegerField(default=0)
    shortage = models.PositiveSmallIntegerField(default=0)
    source_address = models.CharField(max_length=255, editable=False)
    first_octet_address = models.PositiveSmallIntegerField(default=0)
    second_octet_address = models.PositiveSmallIntegerField(default=0)
    third_octet_address = models.PositiveSmallIntegerField(default=0)
    fourth_octet_address = models.PositiveSmallIntegerField(default=0)
    barcode = models.ManyToManyField(Barcode, null=True)

    def __unicode__(self):
        return self.source_address

    class Meta:
        verbose_name = u'Продукт'
        verbose_name_plural = verbose_name + u'ы'
        ordering = ('first_octet_address',
                    'second_octet_address',
                    'fourth_octet_address',
                    'third_octet_address',
                    #'source_address',
        )

    def parse_source_address(self):
        if self.source_address:
            sa = self.source_address.split(' ')[0].split('.')
            if len(sa) == 4:
                return (int(str(octet)) for octet in sa if str(octet).isdigit())

    def save(self, *args, **kwargs):
        if not self.pk:
            try:
                gen = self.parse_source_address()
                self.first_octet_address = gen.next()
                self.second_octet_address = gen.next()
                self.third_octet_address = gen.next()
                self.fourth_octet_address = gen.next()
            except: pass
        super(WarehouseProduct, self).save(*args, **kwargs)

    @property
    def weight(self):
        return self.first_octet_address+self.second_octet_address+self.fourth_octet_address

    def get_nearest_weight(self, lst, last=False):
        if last:
            _lst = sorted([(i.second_octet_address,abs(i.weight-self.weight),i) for i in lst])
        else:
            _lst = ((0,abs(i.weight-self.weight),i) for i in lst)
        return min(_lst)[2]

    def nearest(self, lst):
        u''' ближайший элемент к текущему на складе '''
        _lst = (i for i in lst if i.first_octet_address == self.first_octet_address)
        if self.second_octet_address%2:
            octets = (self.second_octet_address,)
        else:
            octets = (self.second_octet_address, self.second_octet_address+1)
        _lst = [i for i in lst if i.second_octet_address in octets] or lst
        last = not self.second_octet_address in (i.second_octet_address for i in _lst)
        return self.get_nearest_weight(_lst, last)


class WarehouseTransferOrder(models.Model):
    code = models.CharField(max_length=255, unique=True, editable=False)
    document_id = models.CharField(max_length=255, unique=True, editable=False)
    document_number = models.CharField(max_length=255, editable=False)
    document_date = models.DateField(editable=False)
    dest_address = models.CharField(max_length=255, editable=False)
    created = models.DateField(auto_now_add=True, editable=False)
    file_path = models.CharField(max_length=255, null=True, blank=True,
                                editable=False)
    processing_timestamp = models.DateTimeField(null=True, default=None)
    processed = models.BooleanField(default=False)
    reserved = models.BooleanField(default=False)
    products = models.ManyToManyField(  WarehouseProduct,
                                        related_name='wms_order',
                                        null=True)

    class Meta:
        __postfix = u'на складское перемещение'
        verbose_name = u'Заявка %s' % __postfix
        verbose_name_plural = u'Заявки %s' % __postfix
        ordering = ('code', 'created',)

    @property
    def output_file_path(self):
        return settings.DEFAULT_CART_STORAGE + 'order_' + self.document_id
#
#    def xml_data(self):
#        product = etree.Element('product')
#        for field in self._meta.fields:
#            val = getattr(self, field.name)
#            if val and not field.name == 'id':
#                product.attrib[field.name] = str(val)
#                if field.name == 'product':
#                    product.attrib['code'] = val
#        return etree.tostring(product, pretty_print=True)