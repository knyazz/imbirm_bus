#coding: utf-8
__author__ = 'smirnov.ev'
u'''
Складские Телеги.
WMS (Warehouse Management System).
'''
import datetime
from lxml import etree
from watchdog.events import PatternMatchingEventHandler

from .models import WarehouseTransferOrder, WarehouseProduct, Barcode


def xml2WMSmodel_parser(file_path):
    '''
    <XMLSTOREORDER DocumentDescription="Заявка на внутрискладское перемещение" 
    Version="1.00" DateCreation="31.01.14">
    <ORDERHEADER DocumentID="МФ:000007726 31.01.14" DocumentNumber="МФ:000007726"
    DocumentDate="31.01.14">
    <ORDERSTRINGS>
        <ORDERSTRING Product="708725" ProductCaption="Superdefence Суперзащитный
        крем для кожи вокруг глаз SPF 20, 15 мл" ProductRef="7H65010000 " Qty="1"
        SourceAddress="1.17.2.35 " DestAddress="6.6.6.6 ">
            <PRODUCTBARCODE Barcode="020714528508"/>
        </ORDERSTRING>
        <ORDERSTRING Product="35879" ProductCaption="All About Eyes Средство для
        ухода за кожей вокруг глаз 15 мл" ProductRef="61EP010000 " Qty="1" 
        SourceAddress="1.18.3.10 " DestAddress="6.6.6.6 ">
            <PRODUCTBARCODE Barcode="020714157760"/>
        </ORDERSTRING>
        <ORDERSTRING Product="619520" ProductCaption="Lash Power влагостойкая 
        тушь д/ресниц № 01 Black" ProductRef="6LG9010000 " Qty="1" 
        SourceAddress="1.18.1.12 " DestAddress="6.6.6.6 ">
            <PRODUCTBARCODE Barcode="020714303426"/>
            <PRODUCTBARCODE Barcode="20714303426"/>
        </ORDERSTRING>
        <ORDERSTRING Product="771712" ProductCaption="Сверхмягкое жидкое мыло 
        для лица 150 мл" ProductRef="Z3WF010000 " Qty="7" 
        SourceAddress="1.18.3.28 " DestAddress="6.6.6.6 ">
            <PRODUCTBARCODE Barcode="020714665111"/>
        </ORDERSTRING>
        <ORDERSTRING Product="771714" ProductCaption="Сильнодействующее жидкое 
        мыло для жирной кожи 150 мл" ProductRef="Z3WW010000 " Qty="8" 
        SourceAddress="1.18.5.29 " DestAddress="6.6.6.6 ">
            <PRODUCTBARCODE Barcode="020714665227"/>
        </ORDERSTRING>
    </ORDERSTRINGS>
    </ORDERHEADER>
    </XMLSTOREORDER>
    '''
    with open(file_path) as xmlfile:
        _wms = etree.fromstring(xmlfile.read())
        _wms = _wms.getchildren()[0]
        #print _wms.get('DocumentID', '')
        #print _wms.get('DocumentNumber', '')
        #print _wms.get('DocumentDate', datetime.date.today())
        #print _wms.tag
        date = datetime.datetime.strptime(
            _wms.get('DocumentDate', '01.01.2014'),
            '%d.%m.%y')
        wto = dict(
                    document_id =_wms.get('DocumentID', ''),
                    document_number = _wms.get('DocumentNumber', ''),
                    document_date = date,
                    dest_address = _wms.get('DestAddress'),
                    file_path = file_path,
                    code = _wms.get('Barcode', '')
        )
        wto = WarehouseTransferOrder.objects.create(**wto)
        wps = []
        for field in _wms.getchildren():
            for order_string in field:
                #print order_string.get('Product', '')
                #print order_string.get('ProductCaption', '')
                #print order_string.get('ProductRef', '')
                #print order_string.get('Qty', 0)
                #print order_string.get('SourceAddress', '')
                #print order_string.get('DestAddress', '')
                wp = dict(
                            wp_id = order_string.get('Product', ''),
                            description = order_string.get('ProductCaption', ''),
                            ref = order_string.get('ProductRef', ''),
                            qty = order_string.get('Qty', 0),
                            source_address = order_string.get('SourceAddress', ''),
                )
                wp = WarehouseProduct.objects.create(**wp)
                wps.append(wp)
                bars = []
                for bar in order_string.iterchildren():
                    #print bar.get('Barcode', '')
                    _bar, created = Barcode.objects.get_or_create(
                                                code = bar.get('Barcode', '')
                    )
                    bars.append(_bar)
                wp.barcode.add(*bars)
            wto.products.add(*wps)


class FileHandler(PatternMatchingEventHandler):
    u'''
    Проверяем каталог на наличие новых файлов для парсинга
    '''

    patterns = ('*.xml',)
    ignore_directories = True

    def on_created(self, event):
        if not event.is_directory:
            xml2WMSmodel_parser(event.src_path)