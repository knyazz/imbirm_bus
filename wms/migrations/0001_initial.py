# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Barcode'
        db.create_table(u'wms_barcode', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'wms', ['Barcode'])

        # Adding model 'WarehouseProduct'
        db.create_table(u'wms_warehouseproduct', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('wp_id', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('ref', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('qty', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('amount', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('shortage', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('source_address', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('first_octet_address', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('second_octet_address', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('third_octet_address', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('fourth_octet_address', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'wms', ['WarehouseProduct'])

        # Adding M2M table for field barcode on 'WarehouseProduct'
        m2m_table_name = db.shorten_name(u'wms_warehouseproduct_barcode')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('warehouseproduct', models.ForeignKey(orm[u'wms.warehouseproduct'], null=False)),
            ('barcode', models.ForeignKey(orm[u'wms.barcode'], null=False))
        ))
        db.create_unique(m2m_table_name, ['warehouseproduct_id', 'barcode_id'])

        # Adding model 'WarehouseTransferOrder'
        db.create_table(u'wms_warehousetransferorder', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('document_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('document_number', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('document_date', self.gf('django.db.models.fields.DateField')()),
            ('dest_address', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('created', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
            ('file_path', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('processing_timestamp', self.gf('django.db.models.fields.DateTimeField')(default=None, null=True)),
            ('processed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('reserved', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'wms', ['WarehouseTransferOrder'])

        # Adding M2M table for field products on 'WarehouseTransferOrder'
        m2m_table_name = db.shorten_name(u'wms_warehousetransferorder_products')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('warehousetransferorder', models.ForeignKey(orm[u'wms.warehousetransferorder'], null=False)),
            ('warehouseproduct', models.ForeignKey(orm[u'wms.warehouseproduct'], null=False))
        ))
        db.create_unique(m2m_table_name, ['warehousetransferorder_id', 'warehouseproduct_id'])


    def backwards(self, orm):
        # Deleting model 'Barcode'
        db.delete_table(u'wms_barcode')

        # Deleting model 'WarehouseProduct'
        db.delete_table(u'wms_warehouseproduct')

        # Removing M2M table for field barcode on 'WarehouseProduct'
        db.delete_table(db.shorten_name(u'wms_warehouseproduct_barcode'))

        # Deleting model 'WarehouseTransferOrder'
        db.delete_table(u'wms_warehousetransferorder')

        # Removing M2M table for field products on 'WarehouseTransferOrder'
        db.delete_table(db.shorten_name(u'wms_warehousetransferorder_products'))


    models = {
        u'wms.barcode': {
            'Meta': {'object_name': 'Barcode'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'wms.warehouseproduct': {
            'Meta': {'ordering': "('first_octet_address', 'second_octet_address', 'third_octet_address', 'fourth_octet_address', 'source_address')", 'object_name': 'WarehouseProduct'},
            'amount': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'barcode': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['wms.Barcode']", 'null': 'True', 'symmetrical': 'False'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'first_octet_address': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'fourth_octet_address': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'qty': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'ref': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'second_octet_address': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'shortage': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'source_address': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'third_octet_address': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'wp_id': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'wms.warehousetransferorder': {
            'Meta': {'ordering': "('code', 'created')", 'object_name': 'WarehouseTransferOrder'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'created': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'dest_address': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'document_date': ('django.db.models.fields.DateField', [], {}),
            'document_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'document_number': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'file_path': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'processed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'processing_timestamp': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True'}),
            'products': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'wms_order'", 'null': 'True', 'to': u"orm['wms.WarehouseProduct']"}),
            'reserved': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['wms']