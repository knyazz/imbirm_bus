# -*- coding: utf-8 -*-
import time

from django.conf import settings
from django.core.management import BaseCommand

from watchdog.observers import Observer

from wms import FileHandler

class Command(BaseCommand):
    help = 'WMS Xml-file parser daemon. Use: ./manage.py wms_xml_parser'

    def handle(self, *args, **options):
        path = settings.DEFAULT_WMS_XML_STORAGE
        observer = Observer()
        observer.schedule(FileHandler(), path=path, recursive=False)
        observer.start()
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            observer.stop()
        observer.join()