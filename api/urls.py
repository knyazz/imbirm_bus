from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    url(r'^version/', include('api.versions.urls', namespace='version')),
)