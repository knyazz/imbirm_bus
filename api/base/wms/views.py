#coding: utf-8
import datetime

from rest_framework import generics, renderers

import wms

from .serializers import WTOSerializer, WPSerializer


class WMSMixin(object):
    def get_queryset(self):
        return super(WMSMixin, self).get_queryset(
                                    ).select_related(
                                    ).filter(processed=False, reserved=False)

    def filter_queryset(self, queryset):
        return super(WMSMixin, self).filter_queryset(queryset
                                    ).filter(processed=False, reserved=False)

class WTOList(WMSMixin, generics.ListAPIView):
    u'''
        Список заявок на складское перемещение
    '''
    serializer_class = WTOSerializer
    model = wms.models.WarehouseTransferOrder
order_list = WTOList.as_view()


class WTO4items(WTOList):
    paginate_by = 4
order_list_by_4 = WTO4items.as_view()


class ProcessedWTOrder(generics.RetrieveUpdateAPIView):
    model = wms.models.WarehouseTransferOrder
    serializer_class = WTOSerializer

    def post_save(self, obj, created=False):
        if not self.request.DATA.get('reserved'):
            obj.processed = True
            obj.processing_timestamp = datetime.datetime.now()
            data = self.get_serializer(obj).data
            f = renderers.XMLRenderer().render(data)
            with open(obj.output_file_path, 'w') as xmlfile:
                xmlfile.write(f)
processed_wto = ProcessedWTOrder.as_view()


class WTOWPMixin(object):
    def get_queryset(self):
        return super(WTOWPMixin, self).get_queryset(
                                    ).select_related(
                                    )#.filter()

    def filter_queryset(self, queryset):
        return super(WTOWPMixin, self).filter_queryset(queryset
                                    )#.filter()


class WPByWTO(WTOWPMixin, generics.RetrieveUpdateAPIView):
    serializer_class = WPSerializer
    model = wms.models.WarehouseProduct
wto_wp = WPByWTO.as_view()