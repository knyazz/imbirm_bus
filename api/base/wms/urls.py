from django.conf.urls import patterns, url


urlpatterns = patterns('api.base.wms.views',
    url(r'^order_list/$', 'order_list', name='order_list'),
    url(r'^order_list_by_4/$', 'order_list_by_4', name='order_list_by_4'),
    url(r'^processed_wto/(?P<pk>\d+)/$', 'processed_wto', name='processed_wto'),
    url(r'^wto/(?P<wto_pk>\d+)/product/(?P<pk>\d+)/$', 'wto_wp',name='wto_wp'),
)