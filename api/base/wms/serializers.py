#coding: utf-8
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Page

from rest_framework import serializers
from rest_framework.compat import six
from rest_framework.fields import get_component, is_simple_callable

import wms

class BarcodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = wms.models.Barcode


class WPSerializer(serializers.ModelSerializer):
    barcode = BarcodeSerializer(read_only=True)

    class Meta:
        model = wms.models.WarehouseProduct
        read_only_fields = (
                            'wp_id', 'description', 'ref', 'qty',
                            'source_address')


    def resorted_list(self, lst):
        u'''сортировка продуктов на складе в перемещении
            согласно требованиям заказчика
        '''
        #print 'START SORT:'
        if lst:
            i = lst[0]
            sorted_lst = [i]
            lst.remove(i)
            while lst:
                #print 'cur', i
                nearest = i.nearest(lst)
                #print 'nearest', i.nearest(lst)
                sorted_lst.append(nearest)
                lst.remove(nearest)
                #print 'new', lst
                i = nearest
            #print sorted_lst
            #print "============================="
            return sorted_lst
        return lst

    def field_to_native(self, obj, field_name):
        if self.write_only:
            return None
        if self.source == '*':
            return self.to_native(obj)
        # Get the raw field value
        try:
            source = self.source or field_name
            value = obj
            for component in source.split('.'):
                if value is None:
                    break
                value = get_component(value, component)
        except ObjectDoesNotExist:
            return None
        if is_simple_callable(getattr(value, 'all', None)):
            value = self.resorted_list(list(value.all())) if value.exists() else value
            return [self.to_native(item) for item in value]
        if value is None:
            return None
        if self.many is not None:
            many = self.many
        else:
            many = hasattr(value, '__iter__') and not isinstance(value, (Page, dict, six.text_type))
        if many:
            value = self.resorted_list(value)
            return [self.to_native(item) for item in value]
        return self.to_native(value)


class WTOSerializer(serializers.ModelSerializer):
    products = WPSerializer(read_only=False)

    class Meta:
        model = wms.models.WarehouseTransferOrder
        read_only_fields  = (
                            'code', 'document_id', 'document_number',
                            'document_date','created', #'file_path',
                            'processed', 'processing_timestamp',
                            'dest_address'
                            )