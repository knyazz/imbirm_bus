from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    url(r'^wms/', include('api.base.wms.urls', namespace='wms')),
)