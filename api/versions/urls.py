from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    url(r'^latest/', include('api.versions.latest.urls', namespace='latest')),
    url(r'^v1/', include('api.versions.v1.urls', namespace='v1')),
)